const ProductItem = require("../models/ProductItem");
//const UserAccount = require("../models/UserAccount");
const bcrypt = require("bcrypt");
const auth = require("../auth");




// Create a new product

module.exports.addProductItem = (req, res) => {

    const userData = auth.decode(req.headers.authorization);
    let newProduct = new ProductItem({
        productItemName: req.body.productItemName,
        productItemtDescription: req.body.productItemtDescription,
        productItemPrice: req.body.productItemPrice,
        productItemStocks: req.body.productItemStocks
    });

    if (userData.userAccountIsAdmin) 
    {
        return newProduct.save()
            .then(product => {
                res.send({message:`Product creation successful`})
            })
            .catch(error => {
                res.send({ message: `Product creation failed`});
            });
    }
    else 
    {
        return res.status(401).send("You don't have access to this page!");
    };

};




// Retrieve all product admin only
/*
    Steps:
    1. Retrieve all the product (active/inactive) from the database.
    2. Verify the role of the current user (Admin).



*/
module.exports.getAllProducts = (req, res) => {

    const userData = auth.decode(req.headers.authorization);

    if (userData.userAccountIsAdmin) {
        return ProductItem.find({}).then(result => res.send(result));
    }
    else {
        // return res.send(false);
        return res.status(401).send("You don't have access to this page!");
    }

}





// Retrieve All Active Product
/*
    Step:
        1. Retrieve all the product from the database with the property "isActive" true.


*/
module.exports.getAllActive = (req, res) => {
    return ProductItem.find({ productItemIsActive: true }).then(result => res.send(result));
}





// Retrieving a specific product

module.exports.getProduct = (req, res) => {


    return ProductItem.findById(req.params.productItemId).then(result => res.send(result));
}
