const express = require("express");
const router = express.Router();
const userAccountControllers = require("../controllers/userAccountControllers");
const auth = require("../auth");



// Routes for checking email
// router.post("/checkEmail", userAccountControllers.checkEmailExists);

// Route for user registration
router.post("/register", userAccountControllers.registerUserAccount);

// Route for user authentication
router.post("/login", userAccountControllers.loginUser);

// Route for user details
// router.get("/details", auth.verify, userAccountControllers.getProfile);

// Route for enrolling a user
// router.post("/enroll", auth.verify, userAccountControllers.enroll);




////////////////////////////////////////////////////
module.exports = router;
