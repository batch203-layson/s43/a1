const express = require("express");
const router = express.Router();
const productItemControllers = require("../controllers/productItemControllers");
const auth = require("../auth");



// Route for creating a product
router.post("/createproduct", auth.verify, productItemControllers.addProductItem);



// Route for viewing all product (admin only)
router.get("/all", auth.verify, productItemControllers.getAllProducts);


// Route for viewing all active products (all users)
router.get("/", productItemControllers.getAllActive);



// Route for viewing a specific product
router.get("/:productItemId", productItemControllers.getProduct);



////////////////////////////////////////////////////
module.exports = router;
